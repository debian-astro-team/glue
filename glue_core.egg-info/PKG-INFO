Metadata-Version: 2.2
Name: glue-core
Version: 1.22.0
Summary: Core library for the glue multidimensional data visualization project
Home-page: http://glueviz.org
Author: Thomas Robitaille, Chris Beaumont
Author-email: glueviz@gmail.com
Classifier: Intended Audience :: Science/Research
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: 3.13
Classifier: Topic :: Scientific/Engineering :: Visualization
Classifier: License :: OSI Approved :: BSD License
Requires-Python: >=3.10
License-File: LICENSE
Requires-Dist: numpy>=1.17
Requires-Dist: matplotlib>=3.2
Requires-Dist: scipy>=1.1
Requires-Dist: pandas>=1.2
Requires-Dist: echo>=0.6
Requires-Dist: astropy>=4.0
Requires-Dist: fast_histogram>=0.12
Requires-Dist: ipython>=4.0
Requires-Dist: dill>=0.2
Requires-Dist: h5py>=2.10; python_version < "3.11"
Requires-Dist: xlrd>=1.2
Requires-Dist: openpyxl>=3.0
Requires-Dist: mpl-scatter-density>=0.8
Requires-Dist: importlib_resources>=1.3; python_version < "3.9"
Requires-Dist: importlib_metadata>=3.6; python_version < "3.10"
Requires-Dist: shapely>=2.0
Provides-Extra: all
Requires-Dist: scipy; extra == "all"
Requires-Dist: scikit-image; extra == "all"
Requires-Dist: PyAVM; extra == "all"
Requires-Dist: astrodendro; extra == "all"
Requires-Dist: h5py>=2.10; extra == "all"
Requires-Dist: spectral-cube; extra == "all"
Requires-Dist: pillow!=7.1.0; extra == "all"
Provides-Extra: docs
Requires-Dist: sphinx<7; extra == "docs"
Requires-Dist: sphinx-automodapi; extra == "docs"
Requires-Dist: sphinxcontrib-spelling; extra == "docs"
Requires-Dist: numpydoc; extra == "docs"
Requires-Dist: sphinx-book-theme; extra == "docs"
Provides-Extra: astronomy
Requires-Dist: PyAVM; extra == "astronomy"
Requires-Dist: astrodendro; extra == "astronomy"
Requires-Dist: spectral-cube; extra == "astronomy"
Provides-Extra: recommended
Requires-Dist: scikit-image; extra == "recommended"
Provides-Extra: test
Requires-Dist: pytest; extra == "test"
Requires-Dist: pytest-cov; extra == "test"
Requires-Dist: pytest-faulthandler; extra == "test"
Requires-Dist: pytest-flake8; extra == "test"
Requires-Dist: h5py>=2.10; platform_system == "Linux" and extra == "test"
Requires-Dist: objgraph; extra == "test"
Provides-Extra: visualtest
Requires-Dist: pytest-mpl; extra == "visualtest"

|Actions Status| |Coverage Status| |DOI| |User mailing list| |Developer mailing list|

Glue
====

Glue is a python project to link visualizations of scientific datasets
across many files.

|Glue demo|

This repository contains the **glue-core** package which includes much of the core
functionality of glue that is used for the different front-ends, including the Qt-based
application and the Jupyter-based application. Other key repositories include:

* `glue-qt <https://github.com/glue-viz/glue-qt/>`_: the original Qt/desktop application for glue
* `glue-jupyter <https://github.com/glue-viz/glue-jupyter/>`_: a Jupyter front-end for glue

In addition to these, there are a number of plugin packages available. For a full list of repositories,
see https://github.com/glue-viz/.

Features
--------

-  Interactive, linked statistical graphics of multiple files.
-  Support for many `file
   formats <http://www.glueviz.org/en/latest/faq.html#what-data-formats-does-glue-understand>`__
   including common image formats (jpg, tiff, png), ascii tables,
   astronomical image and table formats (fits, vot, ipac), and HDF5.
   Custom data loaders can also be `easily
   added <http://www.glueviz.org/en/latest/customization.html#custom-data-loaders>`__.
-  Highly `scriptable and
   extendable <http://www.glueviz.org/en/latest/coding_with_glue.html>`__.

Installation
------------

For installation documentation, visit
`glueviz.org <http://glueviz.org>`__.

Contributing
------------

If you are interested in contributing to ``glue``, please read our
`Code of Conduct <https://github.com/glue-viz/.github/blob/master/CODE_OF_CONDUCT.md>`_
and `Contribution Guidelines <https://github.com/glue-viz/.github/blob/master/CONTRIBUTING.md>`_.

Support
-------

Please report problems to glueviz@gmail.com, or `open an
issue <https://github.com/glue-viz/glue/issues?state=open>`__.

License
-------

Glue is licensed under the `BSD
License <https://github.com/glue-viz/glue/blob/master/LICENSE>`__.

.. |Actions Status| image:: https://github.com/glue-viz/glue/actions/workflows/ci_workflows.yml/badge.svg
    :target: https://github.com/glue-viz/glue/actions
    :alt: Glue's GitHub Actions CI Status
.. |Coverage Status| image:: https://codecov.io/gh/glue-viz/glue/branch/master/graph/badge.svg
   :target: https://codecov.io/gh/glue-viz/glue
.. |DOI| image:: https://zenodo.org/badge/doi/10.5281/zenodo.13866.svg
   :target: http://dx.doi.org/10.5281/zenodo.13866
.. |User mailing list| image:: http://img.shields.io/badge/mailing%20list-users-green.svg?style=flat
   :target: https://groups.google.com/forum/#!forum/glue-viz
.. |Developer mailing list| image:: http://img.shields.io/badge/mailing%20list-development-green.svg?style=flat
   :target: https://groups.google.com/forum/#!forum/glue-viz-dev
.. |Glue demo| image:: https://raw.githubusercontent.com/glue-viz/glue-qt/main/doc/readme.gif
   :target: http://vimeo.com/53378575
